#!/bin/bash

while true;
    do
    if [ -z "${lastVersion}" ]; then
        lastVersion="0000000000";
    fi;
    if [ ! -f "routes_${lastVersion}.txt" ]; then
        touch routes_${lastVersion}.txt;
    fi;
    currVersion=$(kubectl get configmap ${CONFIGMAP_NAME} -o go-template --template '{{.metadata.resourceVersion}}')
    if [ $? -ne 0 ]; then
        echo "Error getting configmap version from ${CONFIGMAP_NAME}, taking no action";
        sleep 15;
        continue;
    fi;
    if [ "${lastVersion}" == "${currVersion}" ]; then
        lastVersion=${currVersion};
        sleep 5;
        continue;
    fi;
    echo "Change detected to ${CONFIGMAP_NAME} (lastVersion=${lastVersion}, currVersion=${currVersion})!";
    kubectl get configmap ${CONFIGMAP_NAME} -o go-template --template '{{.data.routes}}' | jq -r 'join("\n")' | sort | uniq > routes_${currVersion}.txt;
    if [ "${PIPESTATUS[0]}" -ne 0 ]; then
        echo "Error getting configmap contents from ${CONFIGMAP_NAME}, taking no action";
        sleep 5;
        continue;
    fi;
    for _route in $(comm -23 routes_${lastVersion}.txt routes_${currVersion}.txt); do
        [ ! -z "$(ip route show | grep ${_route} | grep ${GATEWAY})" ] || continue;
        echo "Deleting ${_route} via ${GATEWAY} ...";
        ip route del ${_route} via ${GATEWAY};
    done;
    for _route in $(comm -1 routes_${lastVersion}.txt routes_${currVersion}.txt | awk '{print $1;}'); do
        [ -z "$(ip route show | grep ${_route} | grep ${GATEWAY})" ] || continue;
        echo "Adding ${_route} via ${GATEWAY} ...";
        ip route add ${_route} via ${GATEWAY};
    done;
    rm -f routes_${lastVersion}.txt;
    lastVersion=${currVersion};
done