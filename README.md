# Adding private network static routes via DaemonSet
Based on [jkwong888/k8s-add-static-routes](https://github.com/jkwong888/k8s-add-static-routes). Since its depricated and i need it's functionalty i've adopted the solution and updated the image.

## Synapse
If you use a Cloud provider that dosn't allow you to edit the routing table, or you just need to set one route for a quick debug session this is a convinient way to get it done.

The included DaemonSet discovers the gateway for the private network (i.e. the route for 10.10.10.0/24) and adds the static routes for the on-premise subnets for the private network to the route table.

## DaemonSet Usage

Update the yaml for the [ConfigMap](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/) [routes-configmap.yaml](routes-configmap.yaml) using an editor.  The `routes` key in the ConfigMap contains a JSON array containing an array of on-premise subnets to add to each worker nodes' static route table.

Add this to Kubernetes using the `kubectl` command:

```bash
kubectl create -f routes-configmap.yaml
```

Before using the DaemonSet, we create a [ServiceAccount](https://kubernetes.io/docs/reference/access-authn-authz/service-accounts-admin/), [Role](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#role-and-clusterrole), and [RoleBinding](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#rolebinding-and-clusterrolebinding) to limit the access of the DaemonSet only to the `static-routes` ConfigMap.  Add these resources to Kubernetes:

```bash
kubectl create -f static-route-rbac.yaml
```

Next, create the [DaemonSet](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/) which uses the service account and adds the `CAP_NET_ADMIN` privilege to manipulate each worker node's static route table.

```bash
kubectl create -f add-static-route-daemonset.yaml
```
